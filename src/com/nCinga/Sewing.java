package com.nCinga;

public class Sewing implements Department {

    private int countOfProducts;

    public Sewing(){
        this.countOfProducts = 0;
    }

    public Sewing(int count){
        this.countOfProducts = count;
    }

    private int setCountOfProducts(int count){
        if(count > 0){
            this.countOfProducts = count;
        }
        else {
            this.countOfProducts = 0;
        }
        return this.countOfProducts;
    }

    public int getSewingCountOfProducts(){
        return this.countOfProducts;
    }

    public int increaseProductCount() {
        int count = this.countOfProducts+1;
        return setCountOfProducts(count);
    }

    public int decreaseProductCount() {
        int count = this.countOfProducts-1;
        return setCountOfProducts(count);
    }

    public void printCountOfProduct() {
        System.out.println("Sewing Product Count: "+ getSewingCountOfProducts());
    }
}
