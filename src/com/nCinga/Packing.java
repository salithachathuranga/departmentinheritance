package com.nCinga;

public class Packing implements Department{

    private int countOfProducts;

    public Packing(){
        this.countOfProducts = 0;
    }

    public Packing(int count){
        this.countOfProducts = count;
    }

    private int setCountOfProducts(int count){
        if(count > 0){
            this.countOfProducts = count;
        }
        else {
            this.countOfProducts = 0;
        }
        return this.countOfProducts;
    }

    public int getPackingCountOfProducts(){
        return this.countOfProducts;
    }

    public int increaseProductCount() {
        int count = this.countOfProducts+5;
        return setCountOfProducts(count);
    }

    public int decreaseProductCount() {
        int count = this.countOfProducts-5;
        return setCountOfProducts(count);
    }

    public void printCountOfProduct() {
        System.out.println("Packing Product Count: "+ getPackingCountOfProducts());
    }
}
