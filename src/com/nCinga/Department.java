package com.nCinga;

public interface Department {

    int increaseProductCount();

    int decreaseProductCount();

}
